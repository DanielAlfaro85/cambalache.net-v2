const UI = document.getElementById("main")
var productos = [];

if (localStorage.getItem("productos") != null) {
    productos = JSON.parse(localStorage.getItem("productos"));
}

let usuario = sessionStorage.getItem("usuarioActivo");
let editar = sessionStorage.getItem("editarProducto");
let detalle = sessionStorage.getItem("DetalleProducto");


const PintarDB = () => {
    UI.innerHTML = '';
    productos = JSON.parse(localStorage.getItem("productos"));

    if (productos == null) {
        productos = [];
    } else {
        productos.forEach(element => {
            console.log(element);
            if (element.pNombre == detalle) {
                UI.innerHTML += `<div class="image">
                <img class="img" alt="#"
                    src="${element.pUrl}">
            </div>
            <div class="description">

                <br><br>
                <h1 id="nombreP">${element.pNombre}</h1>
                <br>
                <h2>Ofrecido por: <a class="as" href="#" id="nombreDu">${element.pDuenno}</a></h2>
                <br><br>
                <hr>
                <h2>Descripción</h2>
                <a id="descripcion">${element.pDescripcion}</a>
                <h2>Busco</h2>
                <a id="busco">${element.pBusco}</a>
                <br><br><br>
                <hr id="hr1">
                <br><br><br>
                <button>Agregar</button>

            </div>`
            }
        });
    }
}

document.addEventListener('DOMContentLoaded', PintarDB);