const nomb = document.getElementById("nombre")
const descrip = document.getElementById("textos")
const url = document.getElementById("URL")
const busco = document.getElementById("texto")
const form = document.getElementById("form")


var productos = [];

if (localStorage.getItem("productos") != null) {
    productos = JSON.parse(localStorage.getItem("productos"));
}

let usuario = sessionStorage.getItem("usuarioActivo");
let editar = sessionStorage.getItem("editarProducto");

if (editar != null) {
    productos.forEach(element => {
        if (editar == element.pNombre) {
            document.getElementById("nombre").value = element.pNombre;
            document.getElementById("textos").value = element.pDescripcion;
            document.getElementById("URL").value = element.pUrl;
            document.getElementById("texto").value = element.pBusco;
        }
    });
}

const guardar = () => {
    localStorage.setItem("productos", JSON.stringify(productos));
}

/*const Editar = (actividad) => {
    let nombre;
    let indexProducto = listaProd.findIndex((elemento) => elemento.pNombre == actividad)
    nombre = listaProd[indexProducto].pNombre;
    sessionStorage.setItem("editarProducto", nombre);
    console.log(nombre);
}*/

function Editar() {
    let producto =
    {
        pNombre: document.getElementById("nombre").value,
        pDescripcion: document.getElementById("textos").value,
        pUrl: document.getElementById("URL").value,
        pBusco: document.getElementById("texto").value,
        pDuenno: usuario
    };

    productos.forEach((elemento, index) => {
        if (elemento.pNombre == editar) {
            /*indexProducto = index;*/
            productos[index] = producto;
        }
    });


    console.log(productos);
    guardar();
}

form.addEventListener("submit", e => {
    e.preventDefault()
    Editar();
    window.location.href = "./dashboard.html";
    sessionStorage.removeItem('editarProducto');
})