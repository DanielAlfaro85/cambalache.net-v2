function obtenerListaUsuarios() {
    var listaUsuarios = JSON.parse(localStorage.getItem("usuario"));
    if (listaUsuarios == null) {
        var listaUsuarios = JSON.parse(localStorage.getItem("usuario"));
    }
    return listaUsuarios;
}

function validarCredenciales(pCorreo, pContrasenna) {
    var listaUsuarios = obtenerListaUsuarios();
    var bAcceso = false;
    for (const i of listaUsuarios) {
        if (pCorreo == i.email && pContrasenna == i.pass) {
            bAcceso = true;
            alert("Bienvenido " + i.nombre);
            sessionStorage.setItem("usuarioActivo", i.nombre + " " + i.apellido);
        }
    }
    return bAcceso;
}

/*var usuarios = [];
if (localStorage.getItem("usuario") != null) {
    usuarios = JSON.parse(localStorage.getItem("usuario"));
}*/
var usuarios = [];
function obtenerUsuarios() {
    if (localStorage.getItem("usuario") != null) {
        usuarios = JSON.parse(localStorage.getItem("usuario"));
    }
    return usuarios;
}

function registrarU(nomb, ape, direc, direc2, pais, ciudad, correo, contra) {
    var listaUsuarios = obtenerUsuarios();
    var bAcceso;
    for (const i of listaUsuarios) {
        if (i.email != null && i.email == correo) {
            return false;
        }

    }
    let usuario =
    {
        nombre: nomb,
        apellido: ape,
        direccion1: direc,
        direccion2: direc2,
        country: pais,
        city: ciudad,
        email: correo,
        pass: contra
    };
    sessionStorage.setItem("usuarioActivo", nomb + " " + ape);
    usuarios.push(usuario);
    localStorage.setItem("usuario", JSON.stringify(usuarios));
    bAcceso = true;

    return true;
}

