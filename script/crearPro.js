const nomb = document.getElementById("nombre")
const descrip = document.getElementById("textos")
const url = document.getElementById("URL")
const busco = document.getElementById("texto")
const form = document.getElementById("form")


var productos = [];
if (localStorage.getItem("productos") != null) {
    productos = JSON.parse(localStorage.getItem("productos"));
}

let usuario = sessionStorage.getItem("usuarioActivo");

function guardar_localstorage() {
    let producto =
    {
        pNombre: nomb.value,
        pDescripcion: descrip.value,
        pUrl: url.value,
        pBusco: busco.value,
        pDuenno: usuario
    };

    productos.push(producto);
    localStorage.setItem("productos", JSON.stringify(productos));
}

function obtener() {
    let producto = JSON.parse(localStorage.getItem("productos"));
    console.log(producto);
}

form.addEventListener("submit", e => {
    e.preventDefault()
    guardar_localstorage();
    window.location.href = "./dashboard.html";
})