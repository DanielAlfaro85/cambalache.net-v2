/* Registro de usuarios */
const nomb = document.getElementById("nomb")
const ape = document.getElementById("apell")
const direc = document.getElementById("direc")
const direc2 = document.getElementById("direc2")
const pais = document.getElementById("pais")
const ciudad = document.getElementById("ciudad")
const correo = document.getElementById("correoElec")
const contra = document.getElementById("contrasena")
const form = document.getElementById("form")
var optionHTML = "";

select = document.getElementById("pais");
select.onchange = function () { // Podemos crear una función que reaccione cuando cambia el valor del select como prueba.
    console.log(this.value); //regresa el value del option seleccionado
    console.log(this.innerHTML); //regresa el select completo con todas sus opciones
    //Si no lo quieres usar dentro de una función podrías usar la variable que ya creamos de 'select' en lugar de 'this'
    var options = this.getElementsByTagName("option"); //Te regresa un arrego con todos los options de tu select
    optionHTML = options[this.selectedIndex].innerHTML;  //Te regresa el innetHTML (Maquina 1 o Maquina 2) del option seleccionado
    console.log(optionHTML); //Aquí te queda para quelo uses como gustes!
};

var usuarios = [];
if (localStorage.getItem("usuario") != null) {
    usuarios = JSON.parse(localStorage.getItem("usuario"));
}

function guardar_localstorage() {
    var registro = false;

    registro = registrarU(nomb.value, ape.value, direc.value, direc2.value, optionHTML, ciudad.value, correo.value, contra.value);

    if (registro == false) {
        alert("Este usuario ya se encuentra registrado")
    } else {
        alert("Su cuenta se ha creado exitosamente");
        window.location.href = "./dashboard.html";
    }
    /* let usuario =
     {
         nombre: nomb.value,
         apellido: ape.value,
         direccion1: direc.value,
         direccion2: direc2.value,
         country: optionHTML,
         city: ciudad.value,
         email: correo.value,
         pass: contra.value
     };
     sessionStorage.setItem("usuarioActivo", nomb.value + " " + ape.value);
     usuarios.push(usuario);
     localStorage.setItem("usuario", JSON.stringify(usuarios));*/
}

function obtener() {
    let usuario = JSON.parse(localStorage.getItem("usuario"));
    console.log(usuario);
}

form.addEventListener("submit", e => {
    e.preventDefault()
    guardar_localstorage();
    /*obtener();*/
    /*alert("Su cuenta se ha creado exitosamente");*/
    /*window.location.href = "./dashboard.html";*/
})


