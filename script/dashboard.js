const nav = document.getElementById("n")
const img = document.getElementById("img1")
const nombre = document.getElementById("nombre")
const nombre2 = document.getElementById("nombre2")
const nombre3 = document.getElementById("nombre3")
const UI = document.getElementById("main")
let listaProd = [];
/*let usuario = JSON.parse(localStorage.getItem("usuario"));*/

let usuario = sessionStorage.getItem("usuarioActivo");

console.log(usuario);
nav.innerHTML = '<a id="n">' + usuario + '</a>';

const PintarDB = () => {
    UI.innerHTML = '';

    listaProd = JSON.parse(localStorage.getItem("productos"));

    if (listaProd == null) {
        listaProd = [];
    } else {
        listaProd.forEach(element => {
            console.log(element);
            if (element.pDuenno == usuario) {
                UI.innerHTML += `<div class="container" id="uno">
            <img class="imgrow" alt="#"
                src="${element.pUrl}" id="img1">
            <h1 id="nombre">${element.pNombre}</h1>
            <button class="btn1">Editar</button>
            <br><br><br>
            <br><button class="btn2" id="clear">Eliminar</button>
        </div>`
            }
        });
    }
}

const guardar = () => {
    localStorage.setItem("productos", JSON.stringify(listaProd));

}

document.querySelector('#btn').addEventListener('click', function () {
    window.location.href = "./crudProducto.html";
});

document.addEventListener('DOMContentLoaded', PintarDB);

UI.addEventListener('click', (e) => {
    e.preventDefault();


    if (e.target.innerHTML == 'Eliminar' || e.target.innerHTML == 'Editar') {
        const texto = e.path[1].childNodes[3].innerHTML;
        if (e.target.innerHTML == 'Editar') {
            Editar(texto);
            window.location.href = "./crudProducto2.html";
        }
        if (e.target.innerHTML == 'Eliminar') {
            Eliminar(texto);
            window.location.reload();
        }
    }
});

const Editar = (actividad) => {
    let nombre;
    let indexProducto = listaProd.findIndex((elemento) => elemento.pNombre == actividad)
    nombre = listaProd[indexProducto].pNombre;
    sessionStorage.setItem("editarProducto", nombre);
    console.log(nombre);
} 

const Eliminar = (actividad) => {
    let indexProducto;
    listaProd.forEach((elemento, index) => {
        if (elemento.pNombre == actividad) {
            indexProducto = index;
            console.log(actividad);
        }
    });

    listaProd.splice(indexProducto, 1);
    guardar();
}