const img = document.getElementById("img1")
const nombre = document.getElementById("nombre")
const nombre2 = document.getElementById("nombre2")
const nombre3 = document.getElementById("nombre3")
const UI = document.getElementById("main")
let listaProd = [];
/*let usuario = JSON.parse(localStorage.getItem("usuario"));*/

let usuario = sessionStorage.getItem("usuarioActivo");

console.log(usuario);

const PintarDB = () => {
    UI.innerHTML = '';

    listaProd = JSON.parse(localStorage.getItem("productos"));

    if (listaProd == null) {
        listaProd = [];
    } else {
        listaProd.forEach(element => {
            console.log(element);
            UI.innerHTML += `<div class="container" id="uno">
                <img class="imgrow" alt="#"
                    src="${element.pUrl}">
                <h1>${element.pNombre}</h1>
                <br>
                <p>${element.pDuenno}</p>
            </div>`
        });
    }
}

document.addEventListener('DOMContentLoaded', PintarDB);

UI.addEventListener('click', (e) => {
    e.preventDefault();
    const texto = e.path[1].childNodes[3].innerHTML;
    console.log(texto)
    if (texto != ""){
        Editar(texto);
    }
});

const Editar = (actividad) => {
    let nombre;
    let indexProducto = listaProd.findIndex((elemento) => elemento.pNombre == actividad)
    nombre = listaProd[indexProducto].pNombre;
    sessionStorage.setItem("DetalleProducto", nombre);
    console.log(nombre);
    window.location.href = "./detallePro.html";
} 